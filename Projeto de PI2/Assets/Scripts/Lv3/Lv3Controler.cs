﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Lv3Controler : MonoBehaviour
{
    public int gameTimeLeft;
    public int count = 0;
    public Text gameCountdown;
    public GameObject nav;
    public VideoPlayer dog;
    private bool finish = false;

    // Start is called before the first frame update
    void Start()
    {
        gameTimeLeft = 30; 
        Vector3 temp = new Vector3(0.0f,5f);
        nav.transform.position = temp;
        StartCoroutine("GameStart");
    }

    // Update is called once per frame
    void Update()
    {
        if(nav.transform.position.y > -3.5f){
            nav.transform.Translate(Vector3.down * Time.deltaTime * 0.2f, Space.World);
	        
            if(count > 10 && count < 20){
                nav.transform.Rotate(0, 0, Random.Range(5f, -5f));
            }
            else if(count > 20){
                nav.transform.rotation = Quaternion.identity;
                count = 0;
            }
            count++;
        }
        else{             
            nav.transform.rotation = Quaternion.identity;     
            finish = true;
        }  
    }

    IEnumerator GameStart()
    {
        while(!finish)
        {
            yield return new WaitForSeconds(1);
        }

        dog.Play();
    }
}
