﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour
{
    private Camera cam;
    private float startCamera;
    private float currentCamera;
    public float min;
    public float max;
    public float zoom;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        startCamera = cam.fieldOfView;
        
    }

    // Update is called once per frame
    void Update()
    {
        currentCamera = cam.fieldOfView;
        currentCamera += zoom;
        currentCamera = Mathf.Clamp(currentCamera, min, max);
        cam.fieldOfView = currentCamera;
    }
}
