﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Lv2Controler : MonoBehaviour
{
    private Vector3 _target;
    public Camera Camera;
    public GameObject asteroid;
    public GameObject spaceShip;
    public GameObject moon;
    public Button restartButton;


    public float raioDeSpawn = 5f;
    public float distancia = 100f;

    public int gameTimeLeft;
    int gamecount;
    public AudioSource audio;
    public AudioSource gameOverSound;
    private bool isGameover = false;

    void Start()
    {
        restartButton.gameObject.SetActive(false);
        gameTimeLeft = 66;
        gamecount = 0;
        StartCoroutine("GameStart");
        StartCoroutine("AsteroidStart");
    }

    void Update()
    {
        //MoveShipWithGiro();
        if(!isGameover)
            MoveShipWithMouse();
        if (Input.GetMouseButton(0))
        {
            _target = Camera.ScreenToWorldPoint(Input.mousePosition);
            _target.z = 0;
        }
    }

    public void actionButtonRestart()
    {
        SceneManager.LoadScene ("Fase1");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Asteroid")) {
            gameTimeLeft = 0;
            restartButton.gameObject.SetActive(true);
            audio.Stop();
            gameOverSound.Play();
            isGameover = true;
        }
    }

    void MoveShipWithMouse()
    {
        if (Input.GetMouseButton(0) || Input.touchCount > 0)
        {
            // get mouse position in screen space
            // (if touch, gets average of all touches)
            Vector3 screenPos = Input.mousePosition;
            // set a distance from the camera
            screenPos.z = 10.0f;
            // convert mouse position to world space
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);

            // get current position of this GameObject
            Vector3 newPos = transform.position;
            // set x position to mouse world-space x position
            newPos.x = worldPos.x;
            newPos.y = worldPos.y;
            // apply new position
            spaceShip.transform.position = newPos;
        }

    }

    void MoveShipWithGiro()
    {

        float y = Input.acceleration.y;
        float x = Input.acceleration.x;

        if (Input.touchCount > 0)
        {
            spaceShip.transform.position += new Vector3(x, y, 0);
        }
    }

    void MoveArrow()
    {

        Vector3 temp;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            temp = new Vector3(0, 0.5f, 0);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            temp = new Vector3(0, -0.5f, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            temp = new Vector3(0.5f, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            temp = new Vector3(-0.5f, 0, 0);
        }
        else
        {
            temp = new Vector3(0, 0, 0);
        }
        spaceShip.transform.position += temp;
    }

    void Move(Vector2 direction)
    {

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0.08f, -0.15f));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(0.92f, -0.15f));

        Vector2 pos = transform.position;

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        transform.position = pos;
    }

    IEnumerator GameStart()
    {
        while (gameTimeLeft >= 0)
        {
            yield return new WaitForSeconds(1);
            gameTimeLeft -= 1;
            gamecount += 1;
            Vector3 temp = new Vector3(0, 0, -1f);
            moon.transform.position += temp;
        }

        asteroid.active = false;

        if(!isGameover){
            for(int i = 0; i < 8; i++){
                yield return new WaitForSeconds(1);
            }

            SceneManager.LoadScene ("Fase3");
        }
    }

    IEnumerator AsteroidStart()
    {
        while (true)
        {
            asteroid = ObjectPooling._instance.GetPooledObject(asteroid.tag);
            float posX = Random.Range(-raioDeSpawn, raioDeSpawn);
            float posY = Random.Range(-raioDeSpawn, raioDeSpawn);
            float posZ = Random.Range(-raioDeSpawn, raioDeSpawn);
            asteroid.transform.position = new Vector3(posX, posY, posZ + distancia);

            float sizeX = Random.Range(0.00001f, 0.002f);
            asteroid.transform.localScale += new Vector3(sizeX, sizeX, sizeX);
            gamecount = 0;
            yield return new WaitForSeconds(0.2f);
        }
        asteroid.active = false;
    }

    private void OnGUI()
    {
        GUI.skin.label.fontSize = Screen.width / 40;

        float y = Input.GetAxis("Mouse Y");
        float x = Input.GetAxis("Mouse X");
        if (Input.touchCount > 0) {
            //GUILayout.Label(y.ToString() + "/" + x.ToString());
        }
        
    }
}
