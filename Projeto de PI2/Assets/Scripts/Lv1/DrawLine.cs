﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour {
    private bool isMousePressed;
    private GameObject lineDrawPrefab;
    private LineRenderer lineRenderer;
    private List<Vector3> drawPoints;
    private EdgeCollider2D ec2d;

    void Start () {
        isMousePressed = false;
        drawPoints = new List<Vector3>();
    }
    
    void Update () 
    {
        if(Input.GetMouseButtonDown(0))
        {
            isMousePressed = true;
            lineDrawPrefab = ObjectPooling._instance.GetPooledObject("lineDrawer");
            lineRenderer = lineDrawPrefab.GetComponent<LineRenderer>();
            if(!lineDrawPrefab.active)
            {
                lineDrawPrefab.SetActive(true);
            }

        }
        else if(Input.GetMouseButtonUp(0))
        {
            isMousePressed = false;
            drawPoints.Clear();
            lineRenderer.positionCount = 0;
            if(lineDrawPrefab.active)
            {
                lineDrawPrefab.SetActive(false);
            }
        }

        if(isMousePressed)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!drawPoints.Contains(mousePos)) 
            {
                drawPoints.Add(mousePos);
                lineRenderer.positionCount = drawPoints.Count;
                lineRenderer.SetPosition(drawPoints.Count - 1, mousePos);
            }
        }
    }
    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.gameObject.tag == "points")
        {
            Debug.Log("toquei");
        }
    }
}
