﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    private EdgeCollider2D ec2d;
    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        ec2d = GetComponent<EdgeCollider2D>();
    }

    void Update()
    {
        ec2d.offset = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        rb2d.MovePosition(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if(collider2D.CompareTag("points"))
        {
            Debug.Log("toquei");
        }
    }
}
