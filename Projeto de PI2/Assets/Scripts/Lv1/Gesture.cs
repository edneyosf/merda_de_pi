﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GC
{
    public enum Gestures
    {
        UP,
        DOWN,
        RIGHT,
        LEFT,
        UP_RIGHT,
        UP_LEFT,
        DOWN_RIGHT,
        DOWN_LEFT,
        NONE
    }

    public class Gesture : MonoBehaviour
    {
        private const float MIN_DISTANCE_FOR_GESTURES = 50;

        private static Vector2 lastMousePos;
        private static Gestures lastGesture;

        void Start(){
        }

        private void Update()
        {
            lastGesture = Gestures.NONE;

            if (Input.GetMouseButtonDown(0))
            {
                lastMousePos = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                lastGesture = GetGesture(Input.mousePosition);
            }
        }

        public static bool GetGesture(Gestures gcGesture)
        {
            CheckInstance();

            return gcGesture == lastGesture;
        }

        private static Gestures GetGesture(Vector2 mousePos)
        {
            float distX = mousePos.x - lastMousePos.x;
            float distY = mousePos.y - lastMousePos.y;

            if (distX <= -MIN_DISTANCE_FOR_GESTURES && distY <= -MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.DOWN_LEFT;
            }

            if (distX >= MIN_DISTANCE_FOR_GESTURES && distY <= -MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.DOWN_RIGHT;
            }

            if (distX >= MIN_DISTANCE_FOR_GESTURES && distY >= MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.UP_RIGHT;
            }

            if (distX <= -MIN_DISTANCE_FOR_GESTURES && distY >= MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.UP_LEFT;
            }

            if (distX <= -MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.LEFT;
            }

            if (distX >= MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.RIGHT;
            }

            if (distY <= -MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.DOWN;
            }

            if (distY >= MIN_DISTANCE_FOR_GESTURES)
            {
                return Gestures.UP;
            }

            return Gestures.NONE;
        }

        public static Gestures GetLastGesture()
        {
            CheckInstance();

            return lastGesture;
        }

        private static void CheckInstance()
        {
            if (FindObjectOfType<Gesture>() == null)
            {
                GameObject newGo = new GameObject("GCGestures");
                newGo.AddComponent<Gesture>();
            }
        }
    }
}