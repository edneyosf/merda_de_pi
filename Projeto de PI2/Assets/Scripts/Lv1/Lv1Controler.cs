﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GC;
using System.Threading;
using UnityEngine.SceneManagement;

public class Lv1Controler : MonoBehaviour
{
    public GameObject up;
    public GameObject down;
    public GameObject right;
    public GameObject left;
    private GameObject current;
    public Image greenBar;
    public Image whiteBar;
    public GameObject decolar;
    public GameObject decolando;
    public GameObject explosion;
    public int gameTimeLeft;
    public Text gameCountdown;
    private int direction = 0;
    private bool isFinished = false;
    private bool explodiu = false;
    public AudioSource nasaSound;
    private int countAction = 0;
    public GameObject sucessoText;
    public AudioSource explosionSound;
    public AudioSource gameoverSound;
    public GameObject tryagain;

    void Start()
    {
        greenBar.fillAmount = 0;
        StartCoroutine("CountStart");
        StartCoroutine("GestureStart");
        decolando.SetActive(false);
        explosion.SetActive(false);
        up.SetActive(false);
        down.SetActive(false);
        right.SetActive(false);
        left.SetActive(false);
        tryagain.SetActive(false);
    }

    void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");

        gameCountdown.text = ("Lançamento: " + gameTimeLeft);
        if (gameTimeLeft == 0 && !isFinished)
        {
            decolar.SetActive(false);
            decolando.SetActive(true);
            StopCoroutine("GameStart");

            if (greenBar.fillAmount >= 0 && greenBar.fillAmount <= 0.3)
            {
                Debug.Log("Explodiu");
            }
            else if (greenBar.fillAmount <= 0.6)
            {
                Debug.Log("Explodiu no lançamento");
            }
            else
            {
                Debug.Log("Lançado com sucesso");
            }

            isFinished = true;
        }
        else if (isFinished)
        {
            Debug.Log("decolando...");

            if(gameCountdown.gameObject.activeSelf)
                gameCountdown.gameObject.SetActive(false);
            if (greenBar.enabled)
                greenBar.enabled = false;
            if (whiteBar.enabled)
                whiteBar.enabled = false;
            if(sucessoText.activeSelf)
                sucessoText.SetActive(false);

            if (greenBar.fillAmount >= 0.8 && decolando.transform.position.y < 10)
            {
                decolando.transform.Translate(Vector3.up * Time.deltaTime * 0.7f, Space.World);
            }
            else if(greenBar.fillAmount < 0.8 && !explodiu){
                Debug.Log("Explodiu");
                decolando.SetActive(false);
                explosion.SetActive(false);
                explosion.SetActive(true);
                explodiu = true;
                nasaSound.Stop();
                explosionSound.Play();
                gameoverSound.Play();
                tryagain.SetActive(true);
            }
            else if(greenBar.fillAmount >= 0.8){
                SceneManager.LoadScene ("Fase2");
            }
            else{
                Debug.Log("Acabou");
            }
        }

        if (up.active)
        {
            current = up;

            if (Up() || UpRight() || UpLeft())
            {
                Debug.Log("cima");
                GainPoint();
            }
            else if (Down() || DownRight() || DownLeft() || Right() || Left() || countAction == 2)
            {
                Debug.Log("errou up");
                LosePoint();
            }
        }
        if (down.active)
        {
            current = down;

            if (Down() || DownRight() || DownLeft())
            {
                Debug.Log("baixo");
                GainPoint();
            }
            else if (Up() || UpRight() || UpLeft() || Right() || Left() || countAction == 2)
            {
                Debug.Log("errou down");
                LosePoint();
            }
        }
        if (right.active)
        {
            current = right;
    
            if (Right() || UpRight() || DownRight())
            {
                Debug.Log("direita");
                GainPoint();
            }
            else if (Up() || UpLeft() || Down() || DownLeft() || Left() || countAction == 2)
            {
                Debug.Log("errou right");
                LosePoint();
            }
        }
        if (left.active)
        {
            current = left;

            if (Left() || UpLeft() || DownLeft() )
            {
                Debug.Log("esquerda");
                GainPoint();
            }
            else if(Up() || UpRight() || Down() || DownRight() || Right() || countAction == 2)
            {
                Debug.Log("errou left");
                LosePoint();
            }
        }
    }

    IEnumerator CountStart()
    {
        while(!isFinished)
        {
            if(gameTimeLeft == 25){
                nasaSound.Play();
            }

            gameTimeLeft--;
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator GestureStart()
    {
        while(!isFinished){

            int aux = direction;

            while(aux == direction){
                direction = Random.Range(0, 4);
            }
            Debug.Log("Direcao: "+direction);

            up.SetActive(false);
            down.SetActive(false);
            right.SetActive(false);
            left.SetActive(false);

            if (direction == 0)
            {
                up.SetActive(true);
            }
            else if (direction == 1)
            {
                down.SetActive(true);
            }
            else if (direction == 2)
            {
                right.SetActive(true);
            }
            else if (direction == 3)

            {
                left.SetActive(true);
            }
            countAction++;
            yield return new WaitForSeconds(0.8f);
        }   

        up.SetActive(false);
        down.SetActive(false);
        right.SetActive(false);
        left.SetActive(false); 
    }

    private void GainPoint()
    {
        greenBar.fillAmount += 0.05f;
        current.SetActive(false);
                countAction = 0;
    }

    private void LosePoint()
    {
        greenBar.fillAmount -= 0.05f;
        current.SetActive(false);
        countAction = 0;
    }

    private bool Up()
    {
        return Gesture.GetGesture(Gestures.UP);
    }

    private bool UpRight()
    {
        return Gesture.GetGesture(Gestures.UP_RIGHT);
    }

    private bool UpLeft()
    {
        return Gesture.GetGesture(Gestures.UP_LEFT);
    }

    private bool Down()
    {
        return Gesture.GetGesture(Gestures.DOWN);
    }

    private bool DownRight()
    {
        return Gesture.GetGesture(Gestures.DOWN_RIGHT);
    }

    private bool DownLeft()
    {
        return Gesture.GetGesture(Gestures.DOWN_LEFT);
    }

    private bool Right()
    {
        return Gesture.GetGesture(Gestures.RIGHT);
    }

    private bool Left()
    {
        return Gesture.GetGesture(Gestures.LEFT);
    }

    public void restart(){
        SceneManager.LoadScene ("Fase1");
    }
}
